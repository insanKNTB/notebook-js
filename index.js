
document.addEventListener('DOMContentLoaded', () => {

    var btns = document.querySelector('.btns');

    var textArea = document.querySelector('textarea');

    var loadStoretext = localStorage.getItem('textArea');

    var contolTrue = false;

    textArea.value = loadStoretext;

    btns.addEventListener('click', (event) => {
        event.preventDefault();

        var target = event.target;

        if(target.innerHTML === 'save'){

            localStorage.setItem('textArea', textArea.value);

        } else if (target.innerHTML === 'clear'){

            localStorage.removeItem('textArea');
            textArea.value = '';

        }
    });

    // textArea.addEventListener('keydown', (event) => {
    //     event.preventDefault();

    //     console.log(event.key);


    //     if (event.key === 'Control' || contolTrue){
    //         console.log('work');

    //         contolTrue = true;

    //         if (event.key === 's'){
    //             localStorage.setItem('textArea', textArea.value);
    //         }
    //     }

    // });
});